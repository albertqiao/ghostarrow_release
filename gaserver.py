import asyncio
import logging

from config import Config
import net
from server import GaServer

import uvloop

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

log_fmt = '%(asctime)s-%(filename)s[line:%(lineno)d]-%(levelname)s:%(message)s'
logging.basicConfig(
    filename='./log/server.txt',
    filemode='w',
    level=logging.INFO,
    format=log_fmt)

# read config.json
config = Config('config.json')
serverListenAddr = '0.0.0.0'
serverPort = config.serverPort

# format address
listenAddr = net.Address(serverListenAddr, serverPort)


def run_server():
    loop = asyncio.get_event_loop()
    server = GaServer(loop=loop, listenAddr=listenAddr)
    asyncio.ensure_future(server.listen())
    loop.run_forever()


def main():
    run_server()


if __name__ == '__main__':
    main()
