import asyncio
import logging

from config import Config
import net
from local import GaLocal

import uvloop

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

log_fmt = '%(asctime)s-%(filename)s[line:%(lineno)d]-%(levelname)s:%(message)s'
logging.basicConfig(
    filename='./log/local.txt',
    filemode='w',
    level=logging.INFO,
    format=log_fmt)

# read config.json
config = Config('config.json')
serverAddr = config.serverAddress
serverPort = config.serverPort
localAddr = config.localAddress
localPort = config.localPort

# format addresses
listenAddr = net.Address(localAddr, localPort)
remoteAddr = net.Address(serverAddr, serverPort)


def run_server():
    loop = asyncio.get_event_loop()
    server = GaLocal(loop=loop, listenAddr=listenAddr, remoteAddr=remoteAddr)
    asyncio.ensure_future(server.listen())
    loop.run_forever()


def main():
    run_server()


if __name__ == '__main__':
    main()
